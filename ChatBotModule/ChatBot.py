import telepot
import time
import sys
import pymongo
from threading import Thread

class ChatBot():
	def __init__(self,config):
		self.WELCOME_TEXT = config["welcome_text"]
		self.SUBSCRIBED_TEXT = config["subscribed_text"]
		self.USAGE_TEXT = config["usage_text"]
		self.UNSUBSCRIBED_TEXT = config["unsubscribed_text"]

		self.chatbot_users_database = pymongo.MongoClient(config["database"])["chatbot_users_database"]
		self.active_users_collection = self.chatbot_users_database["active_users_collection"]
		self.pending_users_collection = self.chatbot_users_database["pending_users_collection"]
		
		self.active_users_collection.create_index("chat_id", unique = True)
		self.pending_users_collection.create_index("chat_id", unique = True)

		self.bot = telepot.Bot(config["token"])
		self.next_wakeup = 0
		self.active_user_chat_ids = []

	def on_chat_message(self,msg):
		content_type, chat_type, chat_id = telepot.glance(msg)
		msg["text"] = msg["text"].strip()

		if content_type != "text":
			self.bot.sendMessage(chat_id, "Tracking device's IP address.")
			time.sleep(0.5)
			self.bot.sendMessage(chat_id, "Your IP address has been recorded for further investigation.")
			return

		if self.in_active_user_list(chat_id):
			status = self.is_command(msg["text"])
			if (status):
				if status == 1:
					next_update_timer = int(self.next_wakeup - time.time())
					minute, second = divmod(next_update_timer, 60)
					hour, minute = divmod(minute, 60)
					self.send_message_to_user(
						chat_id, "Service is running. Next update in {} hour(s) {} minute(s) {} second(s).".format(hour, minute, second))

				if status == 2:
					result = self.active_users_collection.delete_many({"chat_id": chat_id})
					self.bot.sendMessage(chat_id, self.UNSUBSCRIBED_TEXT)
					print("[SUCCESS] Removed {} active user: {}".format(result.deleted_count, chat_id))

			else:
				self.send_message_to_user(chat_id, self.USAGE_TEXT)

			return

		if self.in_pending_list(chat_id):
			if self.agree_to_subscribe(msg["text"]):
				try:
					result = self.pending_users_collection.delete_many({"chat_id": chat_id})
					print("[SUCCESS] Removed {} document: {}".format(result.deleted_count, chat_id))
					user = {
						"chat_id": chat_id,
						"first_name": msg["chat"]["first_name"],
						"last_name": msg["chat"]["last_name"]
					}
					entry_id = self.active_users_collection.insert(user)
					print("[SUCCESS] ID: {}\tChat_ID: {}".format(entry_id, chat_id))
					self.bot.sendMessage(chat_id, self.SUBSCRIBED_TEXT)

				except:
					print("[ERROR] Unable to process chat_id: {}".format(chat_id))

				return

			else:
				self.bot.sendMessage(chat_id, "You need to reply /yes to continue.")
				return

		if self.is_first_time(msg["text"]):
			self.bot.sendMessage(chat_id, self.WELCOME_TEXT)
			self.pending_users_collection.insert({"chat_id": chat_id})
			return

	def in_active_user_list(self, chat_id):
		if self.active_users_collection.find_one({"chat_id": chat_id}):
			return True

		return False

	def in_pending_list(self, chat_id):
		if self.pending_users_collection.find_one({"chat_id": chat_id}):
			return True

		return False

	def is_first_time(self, text):
		if text == "/start":
			return True

		else:
			return False

	def is_command(self,text):
		if text == "/status":
			return 1

		if text == "/unsubscribe":
			return 2

		return 0

	def agree_to_subscribe(self,text):
		if text == "/yes":
			return True
		else:
			return False

	def set_active_user_chat_ids(self):
		active_users_count = self.active_users_collection.count()
		self.active_user_chat_ids = []

		cursor = self.active_users_collection.find()

		for user in cursor:
			self.active_user_chat_ids.append(user["chat_id"])

		if (len(self.active_user_chat_ids) != active_users_count):
			print("[WARNING] Document count mismatch.")

	def send_message_to_all(self, message):
		self.set_active_user_chat_ids()

		for chat_id in self.active_user_chat_ids:
			t = Thread(target = self.send_message_to_user,args=(chat_id,message), daemon = True)
			t.start()

		time.sleep(0.05)

	def send_message_to_user(self,chat_id, message):
		try:
			self.bot.sendMessage(chat_id,message)
		except:
			sys.exit(0)


	def send_post_to_all(self, post):
		self.set_active_user_chat_ids()

		chatbot_message_threads = []
		
		for chat_id in self.active_user_chat_ids:
			chatbot_message_threads.append(Thread(target=self.send_post_to_user, args=(chat_id, post), daemon = True))

		for chatbot_message_thread in chatbot_message_threads:
			chatbot_message_thread.start()
			chatbot_message_thread.join()

	def send_post_to_user(self, chat_id, post):
		try:
			caption_text = "{title}\n\nTorrent: {torrent}\n\nGet more info about this torrent on YTS: {page}".format(title = post["title"], torrent = post["torrent_link"], page = post["page"])
			caption_text = caption_text.encode("utf8")
			self.bot.sendPhoto(chat_id, post["movie_art"], caption = caption_text)
		except:
			sys.exit(0)

	



