# YTS Movie Bot

The chatbot will send its subscribers the latest movie torrent posted on YTS.ag. It will periodically parse the RSS feed from YTS.ag and check if any new movies are added. Add the bot on Telegram to see it in action.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. 

### Prerequisites

Before starting, you will need to install the required libraries. Check requirements.txt for the complete list.

```
pip3 install -r requirements.txt
```



In addition to that, you will need to obtain a couple of API Keys

1. Google URL Shortener API-key: 
    ```
    https://developers.google.com/url-shortener/v1/getting_started#APIKey
    ```

2. Telegram Chatbot API-key: 
    ```
    https://telegram.me/BotFather
    ```


Once you get the API-key, replace the relevant fields in "main-config.json" with your own API keys.
```
"googl_api_key": "<your-googl-api-key>"
"token": "<your-telegram-chatbot-token>"
``` 


Lastly start the program, simply run 

```
python3 start_yts_movie_bot.py
```

## Authors

* **Ray Ng**(nhlhanlim93@gmail.com) - *Complete project*







