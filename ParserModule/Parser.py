import feedparser
import time
import sys
import json
import requests
import datetime
import pymongo
from threading import Thread
from bs4 import BeautifulSoup


class Parser(Thread):
	def __init__(self, chat_bot, config):
		Thread.__init__(self)
		self.daemon = True

		self.movie_posts_database = pymongo.MongoClient(config["database"])["movie_posts_database"]
		self.movie_posts_collection = self.movie_posts_database["posts_collection"]
		self.movie_posts_collection.create_index("title", unique=True)
		self.url = config["rss_url"]
		self.refresh_delay	= config["refresh_delay_hour"] * 60 * 60
		self.url_shortener_api_key = config["googl_api_key"]
		self.chat_bot = chat_bot
		
		self.start()

	def run(self):
		while(True):
			try:
				print("[{}] Checking new contents.".format(datetime.datetime.now()))
				self.check_for_new_contents()
				self.chat_bot.next_wakeup = time.time() + self.refresh_delay
				print("[SLEEP] Entering sleep mode")
				time.sleep(self.refresh_delay)
			except (KeyboardInterrupt):
				sys.exit(0)

	def google_url_shorten(self, url):
		req_url = 'https://www.googleapis.com/urlshortener/v1/url?key=' + self.url_shortener_api_key
		payload = {'longUrl': url}
		headers = {'content-type': 'application/json'}
		r = requests.post(req_url, data=json.dumps(payload), headers=headers)
		resp = json.loads(r.text)
		return resp['id']

	def get_title(self, feed, index):
		title = feed['entries'][index]['title']
		return title

	def get_links(self, feed, index):
		page_link= feed['entries'][index]['links'][0]['href']
		torrent_link= feed['entries'][index]['links'][1]['href']
	
		return page_link, torrent_link

	def get_movie_art(self, feed, index):
		soup = BeautifulSoup(feed['entries'][index]['summary_detail']['value'], "html.parser")
		movie_art = soup.a.img["src"]
		return movie_art

	def is_duplicated_post(self, post):
		if self.movie_posts_collection.find_one(post):
			return True

		return False

	def handle_post(self, feed, index):
		post = dict()
		post.update({"title" : self.get_title(feed,index)})

		if self.is_duplicated_post(post):
				print("[DUPLICATE] {}".format(post["title"]))
				return

		page_url, torrent_url = self.get_links(feed,index)
				
		post.update({"page": self.google_url_shorten(page_url)})
		post.update({"torrent_link": self.google_url_shorten(torrent_url)})
		post.update({"movie_art": self.get_movie_art(feed,index)})

		try:
			post_id = self.movie_posts_collection.insert(post)

		except:
			print("[ERROR] Unable to insert post into Database")
			return

		print("[SUCCESS] ID: {}\tMovie: {}".format(post_id, post["title"]))
		self.chat_bot.send_post_to_all(post)


	def check_for_new_contents(self):
		feed = feedparser.parse(self.url)
		
		if not feed['entries']:
			print("[ERROR] Unable to fetch feed.")
			return

		post_threads = []
		
		for i in range(len(feed)):
			post_threads.append(Thread(target = self.handle_post, args = (feed,i), daemon = True))
		
		for post_thread in post_threads:
			post_thread.start()
			post_thread.join()
			time.sleep(0.5)

