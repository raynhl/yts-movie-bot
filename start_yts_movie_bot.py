#!/usr/bin/python

from ChatBotModule.ChatBot import ChatBot
from ParserModule.Parser import Parser
import json


config = json.load(open("main-config.json"))

chat_bot = ChatBot(config["chatbot"])
parser = Parser(chat_bot, config["parser"])

chat_bot.bot.message_loop(chat_bot.on_chat_message, run_forever= True)	
